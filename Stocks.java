public  class Stocks {
    private  String name;
    private double amount;
    private  double price;

    public Stocks(String name, double amount, double price) {
        this.name = name;
        this.amount = amount;
        this.price = price;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getAmount() {
        return amount;
    }

    public  double getPrice() {
        return price;
    }


}







