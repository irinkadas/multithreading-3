public class Buyers  {
    private String name;
    private Integer targetAmount;
    private Integer targetPrice;

    Buyers (String name, Integer amount, Integer price){
        this.name = name;
        this.targetAmount = amount;
        this.targetPrice = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTargetAmount(Integer targetAmount) {
        this.targetAmount = targetAmount;
    }

    public void setTargetPrice(Integer targetPrice) {
        this.targetPrice = targetPrice;
    }

    public String getName() {
        return name;
    }

    public Integer getTargetAmount() {
        return targetAmount;
    }

    public Integer getTargetPrice() {
        return targetPrice;
    }


}
