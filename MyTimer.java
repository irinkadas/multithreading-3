import java.time.LocalTime;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class MyTimer extends TimerTask {
    public static double b = 100;
    public static double c = 100;
    public static double d = 100;
        static  Stocks AAPL = new Stocks("AAPL", 100, 141);
        static Stocks IBM = new Stocks("IMB", 200, 137);
        static Stocks COKE = new Stocks("COKE", 1000, 387);
        static Buyers AliceAAPL = new Buyers("AAPL", 10, 100);
        static Buyers BobAAPL = new Buyers("AAPL", 10, 140);
        static Buyers AliceCOKE = new Buyers("COKE", 20, 390);
        static Buyers BobIBM = new Buyers("IBM", 20, 135);
        static Buyers Charlie = new Buyers("COKE", 300, 370);
    public static double plusMinusInt(double n, int percent) {
        int p = ThreadLocalRandom.current().nextInt(2 * percent + 1) - percent;
        return n * (100.0 + p) / 100.0;
    }
    public static  void updatePrice() {
        for ( int i = 0; i < 1; i++){
           b = plusMinusInt(AAPL.getPrice(), 5);
           c = plusMinusInt(IBM.getPrice(),5);
           d = plusMinusInt(COKE.getPrice(),5);
            System.out.println("Stock " + AAPL.getName() + " changed price " +  b);
            System.out.println("Stock " + IBM.getName() + " changed price " + c);
            System.out.println("Stock " + COKE.getName() + " changed price " + d);}
    }
    public static  void targetToBuyAlice1() {
        if (b <= 100) {
            for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Alice's attempt to buy AAPL stock is successful. 10 shares were bought.");}
        } else {
            for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Alice attempt to buy AAPL stock is unsuccessful");
            }
        }
    }
    public static void targetToBuyAlice2 () {
            if (d >= 390) {
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Alice's attempt to buy COKE stock is successful. 20 shares were bought.");}
            }else {for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Alice attempt to buy COKE stock is unsuccessful");}
            }
    }
    public static void targetToBuyBob1 () {
            if (b <= 140) {
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is successful. 10 shares were bought.");}
            }else { for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Bob attempt to buy AAPL stock is unsuccessful");}
            }
    }
    public static void targetToBuyBob2 () {
            if (c <= 135) {
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is successful.20 shares were bought.");}
            }else {for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Bob attempt to buy IBM stock is unsuccessful");}
            }
    }
    public static void targetToBuyCharlie1 () {
            if (d <= 370) {
                for (int i = 0; i < 1; i++) {
                    System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is successful. 300 shares were bought.");}
            }else {for (int i = 0; i < 1; i++) {
                System.out.println(LocalTime.now() + " Charlie attempt to buy COKE stock is unsuccessful");}
            }
        }

    @Override
    public void run() {
        try {Thread.sleep(10000);
            updatePrice();
        }catch (Exception t){
            t.printStackTrace();
        }
        try {Thread.sleep(5000);
            targetToBuyAlice1();
            targetToBuyAlice2();
            targetToBuyBob1();
            targetToBuyBob2();
            targetToBuyCharlie1();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}



